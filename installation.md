# Installation

## Initial Setup (TODO)
- download and flash firmware and filesystem
- or: rename example.config.json to config.json, build and upload it yourself

## Enduser Setup (Standalone)
1. Scan for access points
1. connect to the access point "illucat" with password "illumination"  
1. open a web browser and navigate to http://192.168.4.1
1. To connect the cat to your own WiFi, open the "Settings" section and change "stationMode" to 1 and set "stationSSID" and "stationPassword" according to your own access point's credentials  
1. submit all changes
1. open the "System" section and hit "Restart"
1. illucat connects to your network and can be reached through http://illucat (might take some time, depending on your DNS server)


## Install and setup Python

### Linux
Download udev rules and follow install instructions in rules file.

```
wget https://raw.githubusercontent.com/platformio/platformio-core/develop/scripts/99-platformio-udev.rules
```

### MacOs
{TBD}

### Windows
download and install python 2.7.x for Windows
https://www.python.org/downloads/

now open your command shell (start->cmd.exe) 
type "python" and hit enter

    python

if it starts up the python shell you can type "exit()" and hit enter
{IMG} python.png

if you get the error message: "'python' is not recognized as an internal or external command,
operable program or batch file" follow the instructions bellow, then resart your shell and retry the python command
https://superuser.com/questions/143119/how-do-i-add-python-to-the-windows-path/143121#143121

## Install  Esptool

now open your command shell (start->cmd.exe) 
type "python -m pip install --upgrade esptool" and hit enter

    python -m pip install --upgrade esptool

{IMG} esptool.png

## Flash Firmware

Extract firmware.bin and spiffs.bin

in the command shell window change to the folder where you extracted the .bin files using cd

flash the firmware with the command "python -m esptool --port **YOUR SERIAL PORT** --baud 115200 write_flash 0x00000 firmware.bin 0x00300000 spiffs.bin"
hint: you can crank the baudrate up to 921600 bps ;)

    python -m esptool --port COM11 --baud 115200 write_flash 0x00000 firmware.bin 0x00300000 spiffs.bin 




