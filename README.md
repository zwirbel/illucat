# Illumination-Cat
The Illumination-Cat (short IlluCat) is the collaborative work of several streams, ranging from 3D modeling, electronics and programming.  
Originating from the Chaos-Drucker Club with the idea of having
a cool status indicator for 3D printers, the project grew beyond the original idea.  
  
From the software point of view, the IlluCat is the exemplary model of the [Sprocket framework](https://gitlab.com/wirelos/sprocket-lib), implementing the various aspects and possibilities of the framework.  
IlluCat can embed several plugins, like web interface, MQTT, IRC or even mesh networking.  
As the framework supports easy integration of many available Arduino libraries, IlluCat can be easily customized to your needs. This repository contains some example implementations that you can use to build your own custom cat.

## OTA
pio run -e build && \
curl -v -F file=@.pioenvs/build/firmware.bin http://illucat.lan/update && \ 
curl -X POST http://illucat.lan/restart 

## Resources & Documentation
[3D Model](https://www.thingiverse.com/thing:2974862)  
[Installation](https://gitlab.com/0x1d/illucat/blob/master/installation.md)  
[API](https://gitlab.com/0x1d/illucat/blob/master/api.md)  
[OctoPrint Stuff](https://github.com/FrYakaTKoP/simple-octo-ws2812)  
[Sprocket framework](https://gitlab.com/wirelos/sprocket-lib)  
[Sprocket plugins](https://gitlab.com/wirelos)
