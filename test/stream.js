const WebSocket = require('ws');
const { times, min, max } = require('ramda');
const cmdWs = new WebSocket('ws://192.168.1.247/stream');
const {Strip} = require('neopixel-utils');

const randomColor = () => Math.floor(Math.random() * 255);

let strip = Strip(8);
//strip.setPixelColor(0, [randomColor(), randomColor(), randomColor()]);
//console.log(strip.getPixelColor(0));

/* const color = (r, g, b) => {
    return uint16_t((r & 0xF8) << 8)
        | uint16_t((g & 0xFC) << 3)
        | uint16_t(b >> 3);
} */

/* const color = (r, g, b) => {
    return ((r & 0xF8) << 8)
        | ((g & 0xFC) << 3)
        | (b >> 3);
} 

const createTest = (ws, items) => () => {
    console.log(`Items: ${items}`)
    let array = new Uint16Array(items);
    times(index => {
        const r = index === 0 ? 255 : Math.floor(Math.random() * 255)
        const g = index === 0 ? 0 : Math.floor(Math.random() * 255)
        const b = index === 0 ? 255 : Math.floor(Math.random() * 255)
        const number = color(r, g, b)
        console.log(`R: ${r}  G: ${g} B: ${b}`)
        //const number = Math.floor(Math.random() * 65535);
        array[index] = number
        console.log(number)
    }
        , items);
    ws.send(array);
}
 */

const createTest = (ws, items) => () => {
    console.log(`Items: ${items}`)
    times(index => {
        strip.setPixelColor(index, [randomColor(), randomColor(), randomColor()]);
    } , items);
    console.log(strip.buffer);
    ws.send(strip.buffer);
}

cmdWs.on('message', console.log);
cmdWs.on('open', createTest(cmdWs, 5));