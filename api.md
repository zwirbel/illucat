# API
Two API architectures are supported: WebSocket and REST.
Both can be used with the same set of fields.  
As everything can be controlled with topics, only a few fields are required:  
| Field | Type | Description |
| ----- | ---- | ---- |
| topic | String | Topic where the payload is dispatched |
| payload | String | Payload to be dispatched |
| broadcast | Integer | Where to send the payload; 0 = local, 1 = broadcast |



- topic
- payload
- broadcast

## Topics
All functionality can be used by sending messages to these topics.  

| topic | type | payload |
| ----- | ---- | ---- |
| pixels/colorWheel | Integer | Value from 0 to 255 to cycle through all colors | 
| pixels/color | Integer | RGB color as integer. By calling this topic, all LEDs of the strip are set synchronously, stopping current running animation. |
| pixels/color2 | Integer |RGB color as integer. Sets the second color used in animations. Does not stop current running animation. |
| pixels/pattern | Integer | Value from 0 to 5 to set the current animation. Available animations: { NONE = 0, RAINBOW_CYCLE = 1, THEATER_CHASE = 2, COLOR_WIPE = 3, SCANNER = 4, FADE = 5 } |
| pixels/totalSteps | Integer | Number of steps of an animation. |
| pixels/brightness | Integer | Integer from 0 to 255 to set the overall brightness of the strip by bitshifting the current colors in memory. Use with caution as running the LEDs on full brightness requires a lot of power.  |

## WebSocket
Endpoint: /ws
Send a JSON String containing the mandatory fields.  

Example:  
``` json
{
    "topic": "pixels/color",
    "payload": 13505813
}
```

## REST
#### Endpoints
Content-Type: application/x-www-form-urlencoded  
POST /pixel/api  
Request: Form post with topic and payload as fields  
Response: 200 + final payload as JSON
   
Examples:  
```shell
# set color by wheel
curl -v -X POST \
    --data "topic=pixels/colorWheel" \
    --data "payload=20" \
    http://illucat/pixel/api  

# set color only on current node
curl -v -X POST \
    --data "topic=pixels/colorWheel" \
    --data "payload=20" \
    --data "broadcast=1"\
    http://illucat/pixel/api  

# set brightness
curl -v -X POST \
    --data "topic=pixels/brightness" \
    --data "payload=10" \
    http://illucat/pixel/api  

# run rainbow pattern
curl -v -X POST \
    --data "topic=pixels/pattern" \
    --data "payload=1" \
    http://illucat/pixel/api   
```