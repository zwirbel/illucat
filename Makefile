PIO ?= platformio

flash: upload uploadfs

install:
	python3 -c "$(curl -fsSL https://raw.githubusercontent.com/platformio/platformio/master/scripts/get-platformio.py)"
init:
	curl -fsSL https://raw.githubusercontent.com/platformio/platformio-core/master/scripts/99-platformio-udev.rules | sudo tee /etc/udev/rules.d/99-platformio-udev.rules
	sudo udevadm control --reload-rules
	sudo udevadm trigger
build:
	$(PIO) run -e build
upload:
	$(PIO) run -e build -t upload
uploadfs:
	$(PIO) run -e build -t uploadfs
