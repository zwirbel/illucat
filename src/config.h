#ifndef __ILLUCAT_CONFIG__
#define __ILLUCAT_CONFIG__

// Scheduler config
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_STD_FUNCTION
#define _TASK_PRIORITY

// Chip config
#define SPROCKET_TYPE       "ILLUCAT"
#define SERIAL_BAUD_RATE    115200
#define STARTUP_DELAY       1000

// Mesh config
#define SPROCKET_MODE       0
#define WIFI_CHANNEL        11
#define MESH_PORT           5555
#define AP_SSID             "illucat"
#define AP_PASSWORD         "illumination"
#define MESH_PREFIX         "illucat-mesh"
#define MESH_PASSWORD       "th3r31sn0sp00n"
#define STATION_SSID        "MyAP"
#define STATION_PASSWORD    "th3r31sn0sp00n"
#define HOSTNAME            "illucat"
#define CONNECT_TIMEOUT  10000
#define MESH_DEBUG_TYPES    ERROR | STARTUP | CONNECTION
//#define MESH_DEBUG_TYPES ERROR | MESH_STATUS | CONNECTION | SYNC | COMMUNICATION | GENERAL | MSG_TYPES | REMOTE

#define PIXEL_CONFIG_FILE "/pixelConfig.json"

// OTA config
#define OTA_PORT 8266
#define OTA_PASSWORD ""

// WebServer
#define WEB_CONTEXT_PATH "/"
#define WEB_DOC_ROOT "/www"
#define WEB_DEFAULT_FILE "index.html"
#define WEB_PORT 80

// NeoPixel
#define LED_STRIP_PIN D2
#define LED_STRIP_LENGTH 8
#define LED_STRIP_BRIGHTNESS 48
#define LED_STRIP_UPDATE_INTERVAL 200
#define LED_STRIP_DEFAULT_COLOR 100
#define COLOR_CONNECTED LED_STRIP_DEFAULT_COLOR
#define COLOR_NOT_CONNECTED 255

#endif