#include "config.h"
#include "WiFiNet.h"
#include "IlluCat.h"

WiFiNet *network;
IlluCat *sprocket;

void setup()
{
    sprocket = new IlluCat(
        {STARTUP_DELAY, SERIAL_BAUD_RATE},
        {WEB_CONTEXT_PATH, WEB_DOC_ROOT, WEB_DEFAULT_FILE, WEB_PORT});
    network = new WiFiNet(
        SPROCKET_MODE,
        STATION_SSID,
        STATION_PASSWORD,
        AP_SSID,
        AP_PASSWORD,
        HOSTNAME,
        CONNECT_TIMEOUT);
    network->connect();
    sprocket->activate();
}

void loop()
{
    sprocket->loop();
    yield();
}