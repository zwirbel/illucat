#include "mqcatt_config.h"
#include <WiFiNet.h>
#include <Sprocket.h>
#include <ESPAsyncWebServer.h>
#include <WebServerConfig.h>
#include <WebServerPlugin.h>
#include <WebConfigPlugin.h>
#include <WebApiPlugin.h>
#include <PixelPlugin.h>
#include <MqttPlugin.h>

WiFiNet *network;
Sprocket *sprocket;
WebServerPlugin *webServerPlugin;
WebConfigPlugin *webConfigPlugin;
WebApiPlugin *webApiPlugin;
PixelPlugin *pixelPlugin;
MqttPlugin *mqttPlugin;

void setup()
{
    sprocket = new Sprocket({STARTUP_DELAY, SERIAL_BAUD_RATE});
    pixelPlugin = new PixelPlugin({LED_STRIP_PIN, LED_STRIP_LENGTH, LED_STRIP_BRIGHTNESS, LED_STRIP_UPDATE_INTERVAL});
    mqttPlugin = new MqttPlugin({MQTT_CLIENT_NAME, MQTT_HOST, MQTT_PORT, MQTT_ROOT_TOPIC});
    webServerPlugin = new WebServerPlugin({WEB_CONTEXT_PATH, WEB_DOC_ROOT, WEB_DEFAULT_FILE, WEB_PORT});
    webConfigPlugin = new WebConfigPlugin(webServerPlugin->server);
    webApiPlugin = new WebApiPlugin(webServerPlugin->server);
    sprocket->addPlugin(pixelPlugin);
    sprocket->addPlugin(webServerPlugin);
    sprocket->addPlugin(webConfigPlugin);
    sprocket->addPlugin(webApiPlugin);
    sprocket->addPlugin(mqttPlugin);

    network = new WiFiNet(
        WIFI_MODE,
        STATION_SSID,
        STATION_PASSWORD,
        AP_SSID,
        AP_PASSWORD,
        HOSTNAME,
        CONNECT_TIMEOUT);
    network->connect();

    webServerPlugin->server->serveStatic(PIXEL_CONFIG_FILE, SPIFFS, "pixelConfig.json");
    webServerPlugin->server->serveStatic(MQTT_CONFIG_FILE, SPIFFS, "mqttConfig.json");

    sprocket->activate();
    sprocket->publish("pixels/pattern", "1");
}

void loop()
{
    sprocket->loop();
    yield();
}