#include "config.h"
#include "Sprocket.h"
#include <MeshNetworkPlugin.cpp>
#include "PixelPlugin.h"

Sprocket *sprocket;
void setup()
{
    sprocket = new Sprocket(
        {STARTUP_DELAY, SERIAL_BAUD_RATE});
    sprocket->addPlugin(new PixelPlugin(
        {LED_STRIP_PIN,
         LED_STRIP_LENGTH,
         LED_STRIP_BRIGHTNESS,
         LED_STRIP_UPDATE_INTERVAL}));
    sprocket->addPlugin(new MeshNetworkPlugin(
        {SPROCKET_MODE, WIFI_CHANNEL,
         MESH_PORT, MESH_PREFIX, MESH_PASSWORD,
         STATION_SSID, STATION_PASSWORD, HOSTNAME,
         MESH_DEBUG_TYPES}));
    sprocket->activate();
}

void loop()
{
    sprocket->loop();
    yield();
}