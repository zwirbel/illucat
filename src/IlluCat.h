#ifndef __ILLUCAT__
#define __ILLUCAT__

#include "config.h"
#include <TaskScheduler.h>
#include <Sprocket.h>

#include "WebServerConfig.h"
#include "WebServerPlugin.h"
#include "WebConfigPlugin.h"
#include "WebApiPlugin.h"
#include "PixelPlugin.h"

using namespace std;
using namespace std::placeholders;

class IlluCat : public Sprocket
{
  public:
    AsyncWebServer *server;
    SprocketConfig sprocketConfig;
    WebServerConfig webConfig;

    IlluCat(SprocketConfig cfg, WebServerConfig webCfg) : Sprocket(cfg)
    {
        sprocketConfig = cfg;
        webConfig = webCfg;
        server = new AsyncWebServer(webConfig.port);
        server->serveStatic(PIXEL_CONFIG_FILE, SPIFFS, "pixelConfig.json");
        addPlugin(new PixelPlugin());
        addPlugin(new WebServerPlugin(webConfig, server));
        addPlugin(new WebConfigPlugin(server));
        addPlugin(new WebApiPlugin(server));
    }

};

#endif